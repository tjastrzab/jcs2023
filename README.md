### Robust models to infer flexible non-deterministic finite automata

**Tomasz Jastrzab, Frédéric Lardeux, Eric Monfroy**

This repository contains the supplementary material accompanying the manuscript submitted to the Journal of Computational Science. 
The supplementary material includes the data table containing the smallest sizes of Nondeterministic Finite Automata found by the proposed k_NFA and (k+1)_NFA inference models. The table lists all samples included in the experimentation, grouped into STAMINA, PEPTIDES, and REGEXES datasets. Columns 3-13 pertain respectively to the following models: 

- ILS(r) - ILS-based model with initial random split, 
- ILS(P*) - ILS-based model with initial split based on the best prefix model, 
- ILS(S*) - ILS-based model with initial split based on the best suffix model,
- P* - the best prefix model,
- S* - the best suffix model.

**Note:** The number of states for (k+1)_NFA includes the (k+1)'th state. Since the (k+1)_NFA can always be reduced to a k_NFA, the smallest NFA found by a (k+1) model has the reported size reduced by 1. The '-' entries denote that no automaton was found within the given time limit.

# Examples

1. Assume that an entry for k_NFA reads 4 states, and the entry for (k+1)_NFA reads 5. It means that both models acted on par, since the (k+1)_NFA can be reduced to a k_NFA of 4 states.
2. Assume that an entry for k_NFA reads 10 states, and the entry for (k+1)_NFA reads 10 states as well. It means that (k+1) model found smaller NFA, because the (k+1)_NFA can be easily reduced to 9 states.
3. Assume that an entry for k_NFA reads 5 states, and the entry for (k+1)_NFA reads 7 states. It means that k model found smaller NFA, because the (k+1)_NFA can only be reduced to 6 states.
4. If an entry for k_NFA reads '-', and the entry for (k+1)_NFA reads some numeric value, it means that (k+1) model was better, because it managed to find any NFA, while the k model did not. In the opposite case, the k model is better. If both entries read '-', it means that they acted on par.
